# L-Systems

The idea is to code some L-Systems and use the turtle python module to draw them.

## What is an L-System?

From [Wikipedia](https://en.wikipedia.org/wiki/L-system):

> An L-system or Lindenmayer system is a parallel rewriting system and a type of
> formal grammar. An L-system consists of an alphabet of symbols that can be used
> to make strings, a collection of production rules that expand each symbol into
> some larger string of symbols, an initial "axiom" string from which to begin
> construction, and a mechanism for translating the generated strings into
> geometric structures.

More examples: http://www.cs.unh.edu/~charpov/programming-lsystems.html

## Usage

```
usage: l-system.py [-h] [--draw | --no-draw] [--save | --no-save] [--output-file OUTPUT_FILE] filename generation

positional arguments:
  filename              Input JSON file to read definitions from
  generation            Number of generations that will be calculated

options:
  -h, --help            show this help message and exit
  --draw, --no-draw     Draw the resulting object with turtle. (default: False)
  --save, --no-save     Save the output of the draw into a file (default: False)
  --output-file OUTPUT_FILE
                        Filename of the output file
```

A set of examples can be found in [./examples/](./examples)

## How To

Symbols The following characters have a geometric interpretation.

```
Character        Meaning
   F	         Move forward by line length drawing a line
   f	         Move forward by line length without drawing a line
   +	         Turn left by turning angle
   -	         Turn right by turning angle
   |	         Reverse direction (ie: turn by 180 degrees)
   [	         Push current drawing state onto stack
   ]	         Pop current drawing state from the stack
   #	         Increment the line width by line width increment
   !	         Decrement the line width by line width increment
   @	         Draw a dot with line width radius
   {	         Open a polygon
   }	         Close a polygon and fill it with fill colour
   >	         Multiply the line length by the line length scale factor
   <	         Divide the line length by the line length scale factor
   &	         Swap the meaning of + and -
   (	         Decrement turning angle by turning angle increment
   )	         Increment turning angle by turning angle increment
```

Ref.: [http://www.paulbourke.net/fractals/lsys/](http://www.paulbourke.net/fractals/lsys/)
