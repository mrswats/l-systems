#!/usr/bin/env python3
from __future__ import annotations

import argparse
import json
import turtle
from tkinter import Canvas
from turtle import Vec2D
from typing import NamedTuple


class InputModel(NamedTuple):
    alphabet: list[str]
    angle: float
    axiom: str
    constants: list[str]
    dist: float
    drawingrules: dict[str, str]
    rules: dict[str, str]

    @classmethod
    def load_model(cls, filename: str) -> InputModel:
        with open(filename) as f:
            raw_json = json.load(f)

        return cls(**raw_json)


class Drawer:
    def __init__(self, model: InputModel) -> None:
        self.dist = model.dist
        self.angle = model.angle
        self.drawing_rules = model.drawingrules
        self.pen = self.setup_turtle()
        self.stack: list[tuple[Vec2D, float]] = []

    def draw(self, sentence: str) -> None:
        for letter in sentence:
            if letter in self.drawing_rules:
                movement = getattr(self, self.drawing_rules.get(letter, ""))
                movement()

        turtle.mainloop()

    def save(self, output_file: str) -> None:
        cv: Canvas = turtle.getcanvas()
        cv.postscript(file=output_file, colormode="color")

    def setup_turtle(self) -> turtle.Turtle:
        pen = turtle.Turtle(visible=False)
        pen.speed("fastest")
        pen.pu()
        pen.goto(0, -turtle.window_height() / 2.5)
        pen.seth(90)
        pen.pd()

        return pen

    def forward(self) -> None:
        return self.pen.forward(self.dist)

    def left(self) -> None:
        return self.pen.left(self.angle)

    def right(self) -> None:
        return self.pen.right(self.angle)

    def push(self) -> None:
        self.stack.append((self.pen.pos(), self.pen.heading()))

    def pop(self) -> None:
        pos, head = self.stack.pop()
        self.pen.pu()
        self.pen.setpos(pos)
        self.pen.seth(head)
        self.pen.pd()


def fractal(
    model: InputModel,
    level: int,
    sentence: str = "",
) -> str:
    sentence = sentence or model.axiom

    # This is so stupid but it works and I love it
    # And the result of several rounds of code golfing
    # after having a working version
    return (
        "".join(
            (
                fractal(model, level - 1, model.rules[letter])
                if letter in model.alphabet
                else letter
            )
            for letter in sentence
        )
        if level > 0
        else sentence
    )


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "filename",
        type=str,
        help="Input JSON file to read definitions from",
    )
    parser.add_argument(
        "generation",
        type=int,
        help="Number of generations that will be calculated",
    )
    parser.add_argument(
        "--draw",
        type=bool,
        default=False,
        required=False,
        action=argparse.BooleanOptionalAction,
        help="Draw the resulting object with turtle.",
    )
    parser.add_argument(
        "--save",
        type=bool,
        default=False,
        required=False,
        action=argparse.BooleanOptionalAction,
        help="Save the output of the draw into a file",
    )
    parser.add_argument(
        "--output-file",
        type=str,
        default="output.ps",
        required=False,
        help="Filename of the output file",
    )

    return parser.parse_args()


def cli(
    filename: str,
    generation: int,
    draw: bool,
    save: bool,
    output_file: str,
) -> None:
    model = InputModel.load_model(filename)

    final_sentence = fractal(model=model, level=generation)

    print(final_sentence)

    if draw:
        turtle = Drawer(model)
        try:
            turtle.draw(final_sentence)
        except (Exception, KeyboardInterrupt):
            pass

        if save:
            turtle.save(output_file)


def main() -> int:
    parsed_args = parse_args()
    cli(
        parsed_args.filename,
        parsed_args.generation,
        parsed_args.draw,
        parsed_args.save,
        parsed_args.output_file,
    )

    return 0


if __name__ == "__main__":
    raise SystemExit(main())
